# HeadHunter App

# Preview
![](https://media.giphy.com/media/wkiPlzeu0DlfDhVv4n/giphy.gif) ![](https://media.giphy.com/media/sKYkaJNOuPZGCAlLgd/giphy.gif) ![](https://media.giphy.com/media/xM4xgcjHjKWRBogs2c/giphy.gif) ![](https://media.giphy.com/media/T98XosRW3redY92K5o/giphy.gif) ![](https://media.giphy.com/media/VzutJco6yIsVNR5HEs/giphy.gif) 

# Description
Implementation of a list with dynamic search based on Paging Library 3, as well as with internal storage using the Room library. The technology and component stack is described below.

# Components
- **Language**: Kotlin
- **Architecture**: MVVM 
- **API**: [hh.ru](https://github.com/hhru/api)
- **Storage**: Room
- **Pagination**: Paging library 3
- **Async**: Kotlin Coroutines, RxKotlin, Live Data
- **Dependency Injection**: Dagger
- **Images**: Glide
- **Network**: Retrofit, OkHttp

*If you have any questions, feel free to ask me and I'll try my utmost to answer you.*
**Telegram [@infernowadays](https://t.me/infernowadays "@infernowadays")**