package com.lazysecs.hh.di

import com.lazysecs.hh.view.base.BaseActivity
import com.lazysecs.hh.view.base.BaseFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        StorageModule::class,
        NavigationModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {
    fun inject(baseActivity: BaseActivity)
    fun inject(baseFragment: BaseFragment)
}