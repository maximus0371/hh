package com.lazysecs.hh.di

import android.content.Context
import androidx.room.Room
import com.lazysecs.hh.BuildConfig
import com.lazysecs.hh.data.db.HhDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesDatabase(context: Context): HhDatabase {
        return Room.databaseBuilder(context, HhDatabase::class.java, BuildConfig.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}