package com.lazysecs.hh.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lazysecs.hh.view.vacancies.cache.CacheVacanciesViewModel
import com.lazysecs.hh.view.vacancies.search.SearchVacanciesViewModel
import com.lazysecs.hh.view.vacancy.VacancyDetailsViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Singleton
class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        viewModels[modelClass]?.get() as T
}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SearchVacanciesViewModel::class)
    internal abstract fun searchVacanciesViewModel(viewModel: SearchVacanciesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CacheVacanciesViewModel::class)
    internal abstract fun cacheVacanciesViewModel(viewModel: CacheVacanciesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VacancyDetailsViewModel::class)
    internal abstract fun vacancyDetailsViewModel(viewModel: VacancyDetailsViewModel): ViewModel
}