package com.lazysecs.hh.di

import com.lazysecs.hh.data.repository.vacancy.VacancyRepository
import com.lazysecs.hh.data.repository.vacancy.VacancyRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindVacancyRepository(
        vacancyRepositoryImpl: VacancyRepositoryImpl
    ): VacancyRepository
}