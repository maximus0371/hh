package com.lazysecs.hh.view.vacancies

interface ItemClickListener {

    fun onItemClicked(itemId: String)
}