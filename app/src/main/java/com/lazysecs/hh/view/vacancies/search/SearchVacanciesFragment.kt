package com.lazysecs.hh.view.vacancies.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import com.lazysecs.hh.databinding.FragmentSearchVacanciesBinding
import com.lazysecs.hh.navigation.Screens
import com.lazysecs.hh.util.PagingLoadStateAdapter
import com.lazysecs.hh.view.base.BaseFragment
import com.lazysecs.hh.view.base.Extras
import com.lazysecs.hh.view.vacancies.ItemClickListener
import com.lazysecs.hh.view.vacancies.adapter.VacancyAdapter
import kotlinx.coroutines.flow.collectLatest

class SearchVacanciesFragment : BaseFragment(), ItemClickListener {

    private var _binding: FragmentSearchVacanciesBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: VacancyAdapter
    private lateinit var viewModel: SearchVacanciesViewModel

    companion object {
        fun newInstance(): SearchVacanciesFragment {
            val fragment = SearchVacanciesFragment()
            val b = Bundle()
            fragment.arguments = b
            return fragment
        }
    }

    override fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentSearchVacanciesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLayout()
        setViewModel()
        setListeners()
        setObservers()
    }

    private fun setListeners() {
        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.length > 1) {
                    viewModel.setSearchQuery(query)
                    binding.recycler.postDelayed({
                        binding.recycler.invalidate()
                        binding.recycler.layoutManager?.scrollToPosition(0)
                    }, 300)
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        binding.search.setOnCloseListener {
            binding.search.clearFocus()
            viewModel.setSearchQuery("")
            false
        }
    }

    override fun onItemClicked(itemId: String) {
        val bundle = Bundle()
        bundle.putString(Extras.VACANCY_ID, itemId)
        navigateTo(Screens.VacancyDetails(bundle))
    }

    private fun setLayout() {
        adapter = VacancyAdapter()
        binding.recycler.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun setViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(SearchVacanciesViewModel::class.java)
    }

    private fun setObservers() {
        viewModel.vacancies.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        with(adapter) {
            binding.swipeRefresh.setOnRefreshListener { refresh() }
            itemClickListener = this@SearchVacanciesFragment

            binding.recycler.adapter = withLoadStateHeaderAndFooter(
                header = PagingLoadStateAdapter(this),
                footer = PagingLoadStateAdapter(this)
            )
            launchOnLifecycleScope {
                loadStateFlow.collectLatest {
                    binding.swipeRefresh.isRefreshing = it.refresh is LoadState.Loading
                }
            }
        }
    }
}