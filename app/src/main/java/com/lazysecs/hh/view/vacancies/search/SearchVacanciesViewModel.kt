package com.lazysecs.hh.view.vacancies.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import androidx.paging.map
import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.data.repository.vacancy.VacancyRepository
import com.lazysecs.hh.view.base.BaseViewModel
import javax.inject.Inject

class SearchVacanciesViewModel @Inject constructor(
    private val repository: VacancyRepository
) : BaseViewModel() {

    private val searchQuery = MutableLiveData("android")

    init {
        setSearchQuery("android")
    }

    val vacancies = searchQuery.switchMap { query ->
        repository.searchVacancies(query).map { pagingData ->
            pagingData.map { vacancyEntity ->
                Vacancy.toVacancy(
                    vacancyEntity
                )
            }
        }.cachedIn(viewModelScope)
    }

    fun setSearchQuery(query: String) {
        searchQuery.value = query
    }
}