package com.lazysecs.hh.view.vacancy

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.lazysecs.hh.R
import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.databinding.FragmentVacancyDetailsBinding
import com.lazysecs.hh.util.StringUtil
import com.lazysecs.hh.view.base.BaseFragment
import com.lazysecs.hh.view.base.Extras


class VacancyDetailsFragment : BaseFragment() {

    private var _binding: FragmentVacancyDetailsBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: VacancyDetailsViewModel
    private var vacancy: Vacancy? = null

    companion object {
        fun newInstance(bundle: Bundle): VacancyDetailsFragment {
            val fragment = VacancyDetailsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentVacancyDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTopMargin(binding.root)
        setViewModel()
        setListeners()
        setObservers()
    }

    override fun setViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(VacancyDetailsViewModel::class.java)
        arguments?.getString(Extras.VACANCY_ID)?.let { viewModel.setVacancyId(it) }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {
        binding.openVacancyInBrowserButton.setOnClickListener {
            launchBrowser()
        }
        binding.back.setOnClickListener {
            back()
        }
    }

    private fun launchBrowser() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(vacancy?.alternateUrl)
        startActivity(intent)
    }

    private fun setObservers() {
        viewModel.vacancyEvent.observe(viewLifecycleOwner, ::onVacancyLoaded)
    }

    private fun onVacancyLoaded(vacancy: Vacancy?) {
        this.vacancy = vacancy

        binding.companyName.text = vacancy?.employer?.name
        binding.vacancyName.text = vacancy?.name
        binding.vacancyDescription.text =
            vacancy?.description?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY) }

        Glide.with(requireContext())
            .load(vacancy?.employer?.logoUrls?.original)
            .centerCrop()
            .placeholder(R.color.design_default_color_primary)
            .into(binding.companyLogo)

        binding.salary.text = String.format(
            "%s - %s %s",
            if (vacancy?.salary?.from == null)
                "..."
            else
                StringUtil().formatMoney(vacancy.salary.from),
            if (vacancy?.salary?.to == null)
                "..."
            else
                StringUtil().formatMoney(vacancy.salary.to),
            if (vacancy?.salary?.currency == null)
                "?"
            else
                vacancy.salary.currency
        )
    }
}