package com.lazysecs.hh.view.vacancy

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.lazysecs.hh.data.db.HhDatabase
import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.data.service.VacancyService
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class VacancyDetailsViewModel @Inject constructor(
    private var database: HhDatabase,
    private var service: VacancyService
) : ViewModel() {

    val vacancyEvent = MutableLiveData<Vacancy>()
    private val compositeDisposable = CompositeDisposable()
    private val vacancyId = MutableLiveData("")

    fun setVacancyId(id: String) {
        vacancyId.value = id
    }

    val vacancy = vacancyId.switchMap { id ->
        database.vacancyDao.getById(id)
    }

    init {
        vacancy.observeForever {
            if (it != null) {
                vacancyEvent.postValue(Vacancy.toVacancy(it))
            } else {
                vacancyId.value?.let { it1 -> loadVacancy(it1) }
            }
        }
    }

    private fun loadVacancy(id: String) {
        compositeDisposable.add(
            service.getVacancyById(id)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    { vacancy -> vacancyEvent.postValue(vacancy) },
                    { throwable -> Timber.e("Throwable %s", throwable.message) })
        )
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}