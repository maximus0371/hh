package com.lazysecs.hh.view.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.lazysecs.hh.view.vacancies.cache.CacheVacanciesFragment
import com.lazysecs.hh.view.vacancies.search.SearchVacanciesFragment

class HomePagerAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle
) : FragmentStateAdapter(fragmentManager, lifecycle) {

    private val numPages = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                SearchVacanciesFragment.newInstance()
            }
            1 -> {
                CacheVacanciesFragment.newInstance()
            }
            else -> SearchVacanciesFragment.newInstance()
        }
    }

    override fun getItemCount(): Int {
        return numPages
    }
}