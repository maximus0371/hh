package com.lazysecs.hh.view.base

interface BackButtonListener {
    fun onBackButtonPressed(): Boolean
}