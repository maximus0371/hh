package com.lazysecs.hh.view.vacancies.cache

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.data.repository.vacancy.VacancyRepository
import com.lazysecs.hh.view.base.BaseViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CacheVacanciesViewModel @Inject constructor(
    private val repository: VacancyRepository
) : BaseViewModel() {

    private lateinit var _vacanciesFlow: Flow<PagingData<VacancyEntity>>
    val vacanciesFlow: Flow<PagingData<Vacancy>>
        get() = _vacanciesFlow.map { pagingData ->
            pagingData.map { vacancyEntity ->
                Vacancy.toVacancy(
                    vacancyEntity
                )
            }
        }

    init {
        getAllVacancies()

    }

    private fun getAllVacancies() = launchPagingAsync({
        repository.getAllVacancies().cachedIn(viewModelScope)
    }, {
        _vacanciesFlow = it
    })
}