package com.lazysecs.hh.view.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.lazysecs.hh.HhApp
import com.lazysecs.hh.util.BarsHeightHelper
import com.lazysecs.hh.view.main.MainActivity
import org.jetbrains.annotations.NotNull
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import javax.inject.Inject

abstract class BaseFragment : Fragment(), BackButtonListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onAttach(@NotNull context: Context) {
        HhApp.instance?.appComponent?.inject(this)

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return getRootView(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewModel()
    }

    abstract fun setViewModel()

    protected abstract fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View?

    protected fun navigateTo(screen: Screen?) {
        val baseActivity = activity as BaseActivity?
        baseActivity?.mRouter?.navigateTo(screen)
    }

    protected fun back() {
        val baseActivity = activity as BaseActivity?
        baseActivity?.onBackPressed()
    }

    protected fun setTopPadding(view: View) {
        view.setPadding(
            view.paddingLeft,
            BarsHeightHelper.getStatusBarHeight(context),
            view.paddingRight,
            view.paddingBottom
        )
    }

    protected fun setTopMargin(view: View) {
        val layoutParams: ViewGroup.MarginLayoutParams =
            view.layoutParams as ViewGroup.MarginLayoutParams
        layoutParams.setMargins(
            layoutParams.leftMargin,
            layoutParams.topMargin + BarsHeightHelper.getStatusBarHeight(context),
            layoutParams.rightMargin,
            layoutParams.bottomMargin
        )
        view.layoutParams = layoutParams
    }

    open fun getRouter(): Router? {
        return if (activity is MainActivity) {
            (activity as MainActivity).router
        } else {
            null
        }
    }

    fun launchOnLifecycleScope(execute: suspend () -> Unit) {
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            execute()
        }
    }

    override fun onBackButtonPressed(): Boolean {
        return if (getRouter() != null) {
            getRouter()?.exit()
            true
        } else {
            false
        }
    }
}