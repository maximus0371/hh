package com.lazysecs.hh.view.vacancies.adapter

import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.databinding.ItemVacancyBinding
import com.lazysecs.hh.util.StringUtil
import com.lazysecs.hh.view.vacancies.ItemClickListener
import javax.inject.Inject

class VacancyAdapter @Inject constructor() :
    PagingDataAdapter<Vacancy, VacancyAdapter.VacancyViewHolder>(VacancyComparator) {

    var itemClickListener: ItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        VacancyViewHolder(
            ItemVacancyBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: VacancyViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class VacancyViewHolder(private val binding: ItemVacancyBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                itemClickListener?.onItemClicked((getItem(absoluteAdapterPosition) as Vacancy).id)
            }
        }

        fun bind(item: Vacancy?) = with(binding) {
            binding.vacancy = item
            binding.executePendingBindings()

            setCurrencyAndAmount(binding.salaryFrom, item?.salary?.currency, item?.salary?.from)
            setCurrencyAndAmount(binding.salaryTo, item?.salary?.currency, item?.salary?.to)
        }

        @BindingAdapter("currency", "amount")
        fun setCurrencyAndAmount(textView: TextView, currency: String?, amount: Int?) {
            val spannableString = SpannableString(
                String.format("%s %s", currency, StringUtil().formatMoney(amount))
            )
            currency?.let { spannableString.setSpan(RelativeSizeSpan(0.6f), 0, it.length, 0) }
            textView.text = if (amount == null) "..." else spannableString
        }
    }

    object VacancyComparator : DiffUtil.ItemCallback<Vacancy>() {
        override fun areItemsTheSame(oldItem: Vacancy, newItem: Vacancy) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Vacancy, newItem: Vacancy) =
            oldItem == newItem
    }
}