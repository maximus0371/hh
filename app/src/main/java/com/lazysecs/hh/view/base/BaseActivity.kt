package com.lazysecs.hh.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lazysecs.hh.HhApp
import com.lazysecs.hh.navigation.SupportFragmentNavigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    lateinit var mRouter: Router

    @Inject
    lateinit var mNavigatorHolder: NavigatorHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        HhApp.instance?.appComponent?.inject(this)
    }

    override fun onResume() {
        super.onResume()
        mNavigatorHolder.setNavigator(SupportFragmentNavigator(this, fragmentContainerId))
    }

    override fun onPause() {
        super.onPause()
        mNavigatorHolder.removeNavigator()
    }

    protected open val fragmentContainerId: Int
        get() = -1
}