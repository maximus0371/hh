package com.lazysecs.hh.view.main

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.lazysecs.hh.R
import com.lazysecs.hh.navigation.Screens
import com.lazysecs.hh.view.base.BaseActivity
import com.lazysecs.hh.view.home.HomeFragment
import com.lazysecs.hh.view.vacancies.search.SearchVacanciesFragment
import ru.terrakok.cicerone.Router

open class MainActivity : BaseActivity() {

    val router: Router?
        get() {
            val fragmentById: Fragment? = supportFragmentManager.findFragmentById(
                fragmentContainerId
            )

            return if (fragmentById is SearchVacanciesFragment) {
                fragmentById.getRouter()
            } else {
                mRouter
            }

        }

    private var backPressed: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRootScreen()
    }

    override val fragmentContainerId: Int
        get() = R.id.fragment_root

    override fun onBackPressed() {
        val fragmentById: Fragment? = supportFragmentManager.findFragmentById(
            fragmentContainerId
        )

        if (fragmentById is HomeFragment) {
            if (backPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed()
                return
            } else {
                Toast.makeText(
                    this,
                    R.string.press_again_to_exit,
                    Toast.LENGTH_SHORT
                ).show()
            }
            backPressed = System.currentTimeMillis()
        } else {
            super.onBackPressed()
        }
    }

    private fun setupRootScreen() {
        mRouter.newRootScreen(Screens.Home())
    }
}