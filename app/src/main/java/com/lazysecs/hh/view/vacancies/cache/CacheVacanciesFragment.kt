package com.lazysecs.hh.view.vacancies.cache

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import com.lazysecs.hh.databinding.FragmentCacheVacanciesBinding
import com.lazysecs.hh.navigation.Screens
import com.lazysecs.hh.util.PagingLoadStateAdapter
import com.lazysecs.hh.view.base.BaseFragment
import com.lazysecs.hh.view.base.Extras
import com.lazysecs.hh.view.vacancies.ItemClickListener
import com.lazysecs.hh.view.vacancies.adapter.VacancyAdapter
import kotlinx.coroutines.flow.collectLatest

class CacheVacanciesFragment : BaseFragment(), ItemClickListener {

    private var _binding: FragmentCacheVacanciesBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: VacancyAdapter
    private lateinit var viewModel: CacheVacanciesViewModel

    companion object {
        fun newInstance(): CacheVacanciesFragment {
            val fragment = CacheVacanciesFragment()
            val b = Bundle()
            fragment.arguments = b
            return fragment
        }
    }

    override fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentCacheVacanciesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLayout()
        setViewModel()
        setObservers()
    }

    override fun onItemClicked(itemId: String) {
        val bundle = Bundle()
        bundle.putString(Extras.VACANCY_ID, itemId)
        navigateTo(Screens.VacancyDetails(bundle))
    }

    private fun setLayout() {
        adapter = VacancyAdapter()
        binding.recycler.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun setViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CacheVacanciesViewModel::class.java)
    }

    private fun setObservers() {
        with(adapter) {
            binding.swipeRefresh.setOnRefreshListener { refresh() }
            itemClickListener = this@CacheVacanciesFragment

            binding.recycler.adapter = withLoadStateHeaderAndFooter(
                header = PagingLoadStateAdapter(this),
                footer = PagingLoadStateAdapter(this)
            )
            with(viewModel) {
                launchOnLifecycleScope {
                    vacanciesFlow.collectLatest { submitData(it) }
                }
                launchOnLifecycleScope {
                    loadStateFlow.collectLatest {
                        binding.swipeRefresh.isRefreshing = it.refresh is LoadState.Loading
                    }
                }
            }
        }
    }
}