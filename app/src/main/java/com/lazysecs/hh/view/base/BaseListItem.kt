package com.lazysecs.hh.view.base

interface BaseListItem<D> {

    fun isSameItem(baseListItem: D): Boolean

    fun isSameContent(baseListItem: D): Boolean {
        return isSameItem(baseListItem)
    }
}