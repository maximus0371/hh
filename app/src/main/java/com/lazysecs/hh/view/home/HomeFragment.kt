package com.lazysecs.hh.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.lazysecs.hh.R
import com.lazysecs.hh.databinding.FragmentHomeBinding
import com.lazysecs.hh.view.base.BaseFragment

class HomeFragment : BaseFragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private var titles: ArrayList<Int> = ArrayList()

    companion object {
        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getRootView(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTopMargin(binding.root)
        setViewPagerAdapter()
        setTabMediator()
    }

    override fun setViewModel() {

    }

    private fun setViewPagerAdapter() {
        binding.viewPager.adapter = HomePagerAdapter(childFragmentManager, lifecycle)
        binding.viewPager.isUserInputEnabled = true
    }

    private fun setTabMediator() {
        titles.add(R.string.search)
        titles.add(R.string.room)

        TabLayoutMediator(
            binding.tabLayout,
            binding.viewPager,
            false,
            true
        ) { tab: TabLayout.Tab, position: Int ->
            tab.setText(
                titles[position]
            )
        }.attach()
    }
}