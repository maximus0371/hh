package com.lazysecs.hh.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.lazysecs.hh.view.main.MainActivity
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Navigator {
    fun getActivityScreen(screen: ScreenEnum?, bundle: Bundle?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getActivityIntent(context: Context): Intent {
                val intent = Intent(context, resolveActivityClass(screen))
                intent.putExtras(bundle!!)
                return intent
            }
        }
    }

    fun getActivityScreen(screen: ScreenEnum?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getActivityIntent(context: Context): Intent {
                return Intent(context, resolveActivityClass(screen))
            }
        }
    }

    fun getActivityScreenWithClearTask(screen: ScreenEnum?): SupportAppScreen {
        return object : SupportAppScreen() {
            override fun getActivityIntent(context: Context): Intent {
                val intent = Intent(context, resolveActivityClass(screen))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                return intent
            }
        }
    }

    fun resolveActivityClass(appScreen: ScreenEnum?): Class<*>? {
        return when (appScreen) {
            ScreenEnum.Main -> MainActivity::class.java
            else -> null
        }
    }

    enum class ScreenEnum {
        Main,
    }
}