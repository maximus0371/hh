package com.lazysecs.hh.navigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.lazysecs.hh.view.home.HomeFragment
import com.lazysecs.hh.view.vacancies.search.SearchVacanciesFragment
import com.lazysecs.hh.view.vacancy.VacancyDetailsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class Home : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return HomeFragment.newInstance()
        }
    }

    class VacancyDetails(var bundle: Bundle) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return VacancyDetailsFragment.newInstance(bundle)
        }
    }
}