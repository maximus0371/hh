package com.lazysecs.hh.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction

import com.lazysecs.hh.R
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class SupportFragmentNavigator(activity: FragmentActivity?, containerId: Int) :
    SupportAppNavigator(activity, containerId) {

    override fun setupFragmentTransaction(
        command: Command,
        currentFragment: Fragment?,
        nextFragment: Fragment?,
        fragmentTransaction: FragmentTransaction
    ) {
        super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction)
        openFromRight(fragmentTransaction)
    }

    private fun openFromRight(fragmentTransaction: FragmentTransaction) {
        fragmentTransaction.setCustomAnimations(
            R.anim.slide_in_from_right, R.anim.slide_out_to_left,
            R.anim.slide_in_from_left, R.anim.slide_out_to_right
        )
    }
}