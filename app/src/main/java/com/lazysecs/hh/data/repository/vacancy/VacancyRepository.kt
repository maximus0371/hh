package com.lazysecs.hh.data.repository.vacancy

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import kotlinx.coroutines.flow.Flow

interface VacancyRepository {
    fun getAllVacancies(): Flow<PagingData<VacancyEntity>>

    fun searchVacancies(searchQuery: String): LiveData<PagingData<VacancyEntity>>
}