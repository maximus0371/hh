package com.lazysecs.hh.data.paging.remotemediator

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.lazysecs.hh.data.db.HhDatabase
import com.lazysecs.hh.data.db.dao.entity.PageKeyEntity
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import com.lazysecs.hh.data.model.PageKey
import com.lazysecs.hh.data.service.VacancyService

@ExperimentalPagingApi
class VacancyRemoteMediator(private val service: VacancyService, private val database: HhDatabase) :
    RemoteMediator<Int, VacancyEntity>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, VacancyEntity>
    ): MediatorResult {

        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> 1
                LoadType.PREPEND ->
                    return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val lastItem = state.lastItemOrNull()
                    val remoteKey: PageKey? = database.withTransaction {
                        if (lastItem?.id != null) {
                            database.pageKeyDao.get(lastItem.id)?.let { PageKey.toPageKey(it) }
                        } else null
                    }

                    if (remoteKey?.nextKey == null) {
                        return MediatorResult.Success(
                            endOfPaginationReached = true
                        )
                    }

                    remoteKey.nextKey
                }
            }

            val response = service.getVacancies(loadKey)
            val vacancies = response.body()?.items

            val isEmpty = vacancies == null || vacancies.isEmpty()
            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    database.vacancyDao.clearAll()
                    database.pageKeyDao.clearAll()
                }

                val prevKey = if (loadKey == 1) null else loadKey - 1
                val nextKey = if (isEmpty) null else loadKey + 1
                val keys = vacancies?.map {
                    PageKey(vacancyId = it.id, prevKey = prevKey, nextKey = nextKey)
                }

                keys?.let {
                    database.pageKeyDao.insertAll(it.map { pageKey ->
                        PageKeyEntity.toPageKeyEntity(
                            pageKey
                        )
                    })
                }

                vacancies?.map { vacancy -> VacancyEntity.toVacancyEntity(vacancy) }?.let {
                    database.vacancyDao.insertAll(
                        it
                    )
                }
            }

            MediatorResult.Success(
                endOfPaginationReached = isEmpty
            )
        } catch (e: Exception) {
            MediatorResult.Error(e)
        }
    }
}