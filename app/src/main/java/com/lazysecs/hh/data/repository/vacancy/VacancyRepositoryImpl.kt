package com.lazysecs.hh.data.repository.vacancy

import androidx.paging.*
import com.lazysecs.hh.data.db.HhDatabase
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import com.lazysecs.hh.data.paging.datasource.VacanciesPagingDataSource
import com.lazysecs.hh.data.paging.remotemediator.VacancyRemoteMediator
import com.lazysecs.hh.data.service.VacancyService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class VacancyRepositoryImpl @Inject constructor(
    private val service: VacancyService,
    private val database: HhDatabase
) : VacancyRepository {

    @ExperimentalPagingApi
    override fun getAllVacancies(): Flow<PagingData<VacancyEntity>> = Pager(
        config = PagingConfig(pageSize = 20),
        remoteMediator = VacancyRemoteMediator(service, database)
    ) {
        database.vacancyDao.getAll()
    }.flow


    @ExperimentalPagingApi
    override fun searchVacancies(searchQuery: String) =
        Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = { VacanciesPagingDataSource(service, searchQuery) }
        ).liveData
}