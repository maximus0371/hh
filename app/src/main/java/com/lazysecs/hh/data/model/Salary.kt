package com.lazysecs.hh.data.model

data class Salary(
    val from: Int?,
    val to: Int?,
    val currency: String?
)

