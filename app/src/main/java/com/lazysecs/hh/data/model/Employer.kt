package com.lazysecs.hh.data.model

import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

data class Employer(
    val name: String?,
    @Embedded(prefix = "logo_urls")
    @SerializedName("logo_urls")
    val logoUrls: LogoUrls?
)