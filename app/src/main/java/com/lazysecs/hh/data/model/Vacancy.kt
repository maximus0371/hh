package com.lazysecs.hh.data.model

import com.google.gson.annotations.SerializedName
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import java.util.*

data class Vacancy(
    val id: String,
    val name: String?,
    val salary: Salary?,
    val url: String?,
    @SerializedName("alternate_url")
    val alternateUrl: String?,
    val employer: Employer?,
    val description: String?,
) {

    companion object {
        fun toVacancy(entity: VacancyEntity): Vacancy {
            return Vacancy(
                id = entity.id,
                name = entity.name,
                salary = entity.salary,
                url = entity.url,
                alternateUrl = entity.alternateUrl,
                employer = entity.employer,
                description = entity.description
            )
        }
    }

    override fun equals(other: Any?): Boolean {
        return id == (other as Vacancy).id
    }

    override fun hashCode(): Int {
        return Objects.hash(id, name, url)
    }
}