package com.lazysecs.hh.data.db.dao

import androidx.lifecycle.LiveData
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import io.reactivex.rxjava3.core.Single

@Dao
interface VacancyDao {

    @Query("SELECT * FROM vacancy")
    fun getAll(): PagingSource<Int, VacancyEntity>

    @Query("SELECT * FROM vacancy WHERE id == :id")
    fun getById(id: String): LiveData<VacancyEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entities: List<VacancyEntity>)

    @Query("DELETE FROM vacancy")
    suspend fun clearAll()
}