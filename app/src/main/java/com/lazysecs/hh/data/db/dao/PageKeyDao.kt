package com.lazysecs.hh.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lazysecs.hh.data.db.dao.entity.PageKeyEntity

@Dao
interface PageKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(keyEntities: List<PageKeyEntity>)

    @Query("SELECT * FROM pageKey WHERE vacancyId LIKE :vacancyId")
    fun get(vacancyId: String): PageKeyEntity?

    @Query("DELETE FROM pageKey")
    suspend fun clearAll()
}