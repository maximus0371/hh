package com.lazysecs.hh.data.model

import com.google.gson.annotations.SerializedName

data class LogoUrls(
    var original: String?,
    @SerializedName("240")
    var size240: String?,
    @SerializedName("90")
    var size90: String?
)