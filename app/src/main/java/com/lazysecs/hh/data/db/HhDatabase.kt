package com.lazysecs.hh.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lazysecs.hh.data.db.dao.PageKeyDao
import com.lazysecs.hh.data.db.dao.VacancyDao
import com.lazysecs.hh.data.db.dao.entity.PageKeyEntity
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity

@Database(
    entities = [VacancyEntity::class, PageKeyEntity::class],
    version = 1,
    exportSchema = false
)
abstract class HhDatabase : RoomDatabase() {

    abstract val vacancyDao: VacancyDao
    abstract val pageKeyDao: PageKeyDao
}