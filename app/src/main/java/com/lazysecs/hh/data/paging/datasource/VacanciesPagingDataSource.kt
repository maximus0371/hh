package com.lazysecs.hh.data.paging.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.lazysecs.hh.data.db.dao.entity.VacancyEntity
import com.lazysecs.hh.data.service.VacancyService

class VacanciesPagingDataSource(
    private val service: VacancyService,
    private val searchQuery: String
) :
    PagingSource<Int, VacancyEntity>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, VacancyEntity> {
        val pageNumber = params.key ?: 1

        return try {
            val response = service.searchVacancies(pageNumber, searchQuery)
            val vacancies = response.body()?.items

            LoadResult.Page(
                data = vacancies!!.map { vacancy -> VacancyEntity.toVacancyEntity(vacancy) },
                prevKey = if (pageNumber == 1) null else pageNumber - 1,
                nextKey = if (vacancies.isEmpty()) null else pageNumber + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, VacancyEntity>): Int = 1
}