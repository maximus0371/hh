package com.lazysecs.hh.data.db.dao.entity

import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.lazysecs.hh.data.model.Employer
import com.lazysecs.hh.data.model.Salary
import com.lazysecs.hh.data.model.Vacancy

@Entity(tableName = "vacancy")
class VacancyEntity {

    @NonNull
    @PrimaryKey
    lateinit var id: String

    var name: String? = null
    var url: String? = null
    var alternateUrl: String? = null

    var description: String? = null

    @Embedded(prefix = "employer")
    var employer: Employer? = null

    @Embedded(prefix = "salary")
    var salary: Salary? = null

    companion object {
        fun toVacancyEntity(vacancy: Vacancy): VacancyEntity {
            val entity = VacancyEntity()
            entity.id = vacancy.id
            entity.name = vacancy.name
            entity.salary = vacancy.salary
            entity.url = vacancy.url
            entity.alternateUrl = vacancy.alternateUrl
            entity.employer = vacancy.employer
            entity.description = vacancy.description

            return entity
        }
    }
}