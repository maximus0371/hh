package com.lazysecs.hh.data.service

import com.lazysecs.hh.data.model.Vacancy
import com.lazysecs.hh.data.service.responses.VacancyResponseModel
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface VacancyService {

    @GET("vacancies/")
    suspend fun getVacancies(
        @Query("page") page: Int
    ): Response<VacancyResponseModel>

    @GET("vacancies/")
    suspend fun searchVacancies(
        @Query("page") page: Int,
        @Query("text") text: String
    ): Response<VacancyResponseModel>

    @GET("vacancies/{vacancyId}/")
    fun getVacancyById(
        @Path("vacancyId") vacancyId: String
    ): Single<Vacancy>
}