package com.lazysecs.hh.data.model

import com.lazysecs.hh.data.db.dao.entity.PageKeyEntity

data class PageKey(
    val vacancyId: String,
    val prevKey: Int?,
    val nextKey: Int?
) {
    companion object {
        fun toPageKey(entity: PageKeyEntity): PageKey {
            return PageKey(
                entity.vacancyId,
                entity.prevKey,
                entity.nextKey
            )
        }
    }
}