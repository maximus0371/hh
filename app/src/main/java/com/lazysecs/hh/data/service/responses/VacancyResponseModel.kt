package com.lazysecs.hh.data.service.responses

import com.lazysecs.hh.data.model.Vacancy

class VacancyResponseModel {
    var items: List<Vacancy>? = null
}