package com.lazysecs.hh.data.db.dao.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.lazysecs.hh.data.model.PageKey

@Entity(tableName = "pageKey")
class PageKeyEntity {
    @PrimaryKey
    @NonNull
    lateinit var vacancyId: String

    var prevKey: Int? = null

    var nextKey: Int? = null

    companion object {
        fun toPageKeyEntity(pageKey: PageKey): PageKeyEntity {
            val entity = PageKeyEntity()
            entity.vacancyId = pageKey.vacancyId
            entity.prevKey = pageKey.prevKey
            entity.nextKey = pageKey.nextKey

            return entity
        }
    }
}