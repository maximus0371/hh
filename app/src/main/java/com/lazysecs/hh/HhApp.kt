package com.lazysecs.hh

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.lazysecs.hh.di.AppComponent
import com.lazysecs.hh.di.AppModule
import com.lazysecs.hh.di.DaggerAppComponent
import com.lazysecs.hh.di.NetworkModule
import timber.log.Timber
import timber.log.Timber.DebugTree

class HhApp : Application() {

    private var mAppComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        mAppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule())
            .build()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        setupTimber()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

    val appComponent: AppComponent?
        get() = mAppComponent

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }

    companion object {
        var instance: HhApp? = null
            private set
    }
}
