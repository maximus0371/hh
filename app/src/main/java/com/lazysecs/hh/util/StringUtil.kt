package com.lazysecs.hh.util

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class StringUtil {

    fun formatMoney(amount: Int?): String? {
        val symbols: DecimalFormatSymbols = DecimalFormatSymbols.getInstance()
        symbols.groupingSeparator = ' '
        val formatter = DecimalFormat("###,###", symbols)

        return if (amount == null) "" else formatter.format(amount)
    }
}