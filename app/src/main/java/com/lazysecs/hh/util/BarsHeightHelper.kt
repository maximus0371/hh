package com.lazysecs.hh.util

import android.content.Context

object BarsHeightHelper {

    private var STATUS_BAR: Int? = null

    fun getStatusBarHeight(context: Context?): Int {
        if (STATUS_BAR == null) {
            var statusBarHeight = 0
            val resourceId =
                context?.resources?.getIdentifier(
                    "status_bar_height",
                    "dimen",
                    "android"
                )
            if (resourceId!! > 0) {
                statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
            }
            STATUS_BAR = statusBarHeight
        }
        return STATUS_BAR!!
    }
}